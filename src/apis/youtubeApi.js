import axios from 'axios';

const API_KEY = "AIzaSyBZmnu12XP1Hmxlfv7g0rhx44_E5aXog_k"

export default axios.create(
    {
        baseURL: 'https://www.googleapis.com/youtube/v3',
        params: {
            key: API_KEY,
            type: 'video',
            part: 'snippet',
        }
    }
)